#-------------------------------------------------
#
# Project created by QtCreator 2014-06-04T10:42:41
#
#-------------------------------------------------

QT       += core gui network opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GalaxyDefender-Klient
TEMPLATE = app


SOURCES += \
    main.cpp \
    mainwindow.cpp \
    myglwidget.cpp \
    obiekt.cpp

HEADERS  += \
    mainwindow.h \
    myglwidget.h \
    obiekt.h

FORMS    += \
    mainwindow.ui

OTHER_FILES += \
    0.png \
    1.png \
    2.png \
    3.png \
    4.png \
    t1.png \
    t2.png \
    t3.png \
    t4.png \
    t5.png \
    t6.png
