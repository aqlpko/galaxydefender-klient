#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork>
#include <QKeyEvent>
#include "obiekt.h"
#include <QTimer>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void keyPressEvent(QKeyEvent* k);
    void keyReleaseEvent(QKeyEvent* k);
    void klawisze();

private slots:
    void dataReceived();
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QTcpSocket* socket;
    bool w;
    bool s;
    bool d;
    bool a;
    bool l;
signals:
};

#endif // MAINWINDOW_H
