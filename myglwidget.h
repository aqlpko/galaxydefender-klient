#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QGLWidget>
#include <QTimer>
#include <QList>
#include "obiekt.h"

class MyGLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit MyGLWidget(QWidget *parent = 0);
    void dodajPocisk(QString dane, int n);
    void dodajWrog1(QString dane, int n);
    void dodajWrog2(QString dane, int n);
    void dodajWrog3(QString dane, int n);
    void dodajGracz1(QString dane);
    void dodajGracz2(QString dane);
    void setTlo(QString dane);
    void usun(int* n);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    GLfloat backgroundColor[4];
    GLfloat foregroundColor[4];
    QList<Pocisk*> pocisk;
    QList<Wrog1*> wrog1;
    QList<Wrog2*> wrog2;
    QList<Wrog3*> wrog3;
    Gracz* gracz1;
    Gracz* gracz2;
    Tlo* tlo;

signals:

public slots:

};

#endif // MYGLWIDGET_H
