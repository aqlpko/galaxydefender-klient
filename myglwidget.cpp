#include "myglwidget.h"
#include "GL/glu.h"
MyGLWidget::MyGLWidget(QWidget *parent) :
    QGLWidget(parent)
{
    foregroundColor[0]=0;
    foregroundColor[1]=1;
    foregroundColor[2]=0;
    foregroundColor[3]=1;


    backgroundColor[0]=0.8;
    backgroundColor[1]=0.8;
    backgroundColor[2]=0.8;
    backgroundColor[3]=1;

}
void MyGLWidget::initializeGL()
{

    glClearColor(backgroundColor[0],backgroundColor[1],backgroundColor[2],backgroundColor[3]);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    gracz1=new Gracz();
    gracz2=new Gracz();
    tlo=new Tlo();
}
void MyGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0,0,-10);

    for(int i=0;i<pocisk.size();i++)
       pocisk[i]->rysuj();
    for(int i=0;i<wrog1.size();i++)
       wrog1[i]->rysuj();
    for(int i=0;i<wrog2.size();i++)
       wrog2[i]->rysuj();
    for(int i=0;i<wrog3.size();i++)
       wrog3[i]->rysuj();

    gracz1->rysuj();
    gracz2->rysuj();
    tlo->rysuj();
}
void MyGLWidget::resizeGL(int width, int height)
{
        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, 0.75, 0, 1, 4.0, 15.0);
        glMatrixMode(GL_MODELVIEW);
}

void MyGLWidget::dodajPocisk(QString dane, int n)
{
    if(pocisk.size()>=n+1)
        pocisk[n]->setPocisk(dane);
    else
    {
        Pocisk* nowy=new Pocisk(dane);
        pocisk.append(nowy);
    }
}

void MyGLWidget::dodajWrog1(QString dane, int n)
{
    if(wrog1.size()>=n+1)
        wrog1[n]->setDane(dane);
    else
    {
    Wrog1* nowy=new Wrog1(dane);
    wrog1.append(nowy);
    }
}

void MyGLWidget::dodajWrog2(QString dane, int n)
{
    if(wrog2.size()>=n+1)
        wrog2[n]->setDane(dane);
    else
    {
    Wrog2* nowy=new Wrog2(dane);
    wrog2.append(nowy);
    }
}

void MyGLWidget::dodajWrog3(QString dane, int n)
{
    if(wrog3.size()>=n+1)
        wrog3[n]->setDane(dane);
    else
    {
    Wrog3* nowy=new Wrog3(dane);
    wrog3.append(nowy);
    }
}

void MyGLWidget::dodajGracz1(QString dane)
{
    gracz1->setGracz(dane);
}
void MyGLWidget::dodajGracz2(QString dane)
{
    gracz2->setGracz(dane);
}

void MyGLWidget::usun(int *n)
{
    for(int i=n[0];i<pocisk.size();i++)
    {
        delete pocisk[i];
        pocisk.takeAt(i);
    }
    for(int i=n[1];i<wrog1.size();i++)
    {
        delete wrog1[i];
        wrog1.takeAt(i);
    }
    for(int i=n[2];i<wrog2.size();i++)
    {
        delete wrog2[i];
        wrog2.takeAt(i);
    }
    for(int i=n[3];i<wrog3.size();i++)
    {
        delete wrog3[i];
        wrog3.takeAt(i);
    }
}
void MyGLWidget::setTlo(QString dane)
{
    tlo->setTlo(dane.toInt());
}
