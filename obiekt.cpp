#include "obiekt.h"
#include <QFile>
Obiekt::Obiekt()
{
    for(int i=0;i<2;i++)
    {
        pos1[i]=0;
        pos2[i]=0;
        pos3[i]=0;
        pos[i]=0;
    }
    r=0;
    w=0;
    h=0;
}
Obiekt::~Obiekt(){}
void Obiekt::load_tekstura(QString adres)
{
    plik.load(adres);
    plik=QGLWidget::convertToGLFormat(plik);
    w=plik.width();
    h=plik.height();
    glGenTextures(1,&tekstura);
    glBindTexture(GL_TEXTURE_2D,tekstura);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,w,h,0,GL_RGBA,GL_UNSIGNED_BYTE,(GLvoid*) plik.bits());
}
void Obiekt::setDane(QString dane)
{
    QTextStream in(&dane);
    QString liczba;
    in>>liczba;
    r=liczba.toFloat();
    in>>liczba;
    pos[0]=liczba.toFloat();
    in>>liczba;
    pos[1]=liczba.toFloat();
    oblicz_pos();
}
Tlo::Tlo()
{
    typ=0;
    last_typ=0;
    load_tekstura("0.png");
}
void Tlo::setTlo(int typ_)
{
    typ=typ_;
    if(last_typ!=typ)
        switch(typ)
        {
        case 0:
        {
            load_tekstura("0.png");
            break;
        }
        case 1:
        {
            load_tekstura("1.png");
            break;
        }
        case 2:
        {
            load_tekstura("2.png");
            break;
        }
        case 3:
        {
            load_tekstura("3.png");
            break;
        }
        case 4:
        {
            load_tekstura("4.png");
            break;
        }
        }
    last_typ=typ;
}

Tlo::~Tlo(){}
void Tlo::rysuj()
{
    glBindTexture(GL_TEXTURE_2D,tekstura);
    glColor4f(1,1,1,1);
    glBegin(GL_POLYGON);
    glTexCoord2f(0,0);
    glVertex2f(0,0);
    glTexCoord2f(1,0);
    glVertex2f(0.75,0);
    glTexCoord2f(1,1);
    glVertex2f(0.75,1);
    glTexCoord2f(0,1);
    glVertex2f(0,1);
    glEnd();
}
void Tlo::oblicz_pos(){}
Pocisk::Pocisk():Obiekt()
{
    for(int i=0;i<2;i++)
        pos4[i]=0;
    color[0]=0;
    color[1]=0;
    color[2]=0;
    color[3]=0;
}
Pocisk::Pocisk(QString dane)
{
    setPocisk(dane);
}
void Pocisk::setPocisk(QString dane)
{
    QTextStream in(&dane);
    QString liczba;
    in>>liczba;
    r=liczba.toFloat();
    in>>liczba;
    pos[0]=liczba.toFloat();
    in>>liczba;
    pos[1]=liczba.toFloat();
    in>>liczba;
    color[0]=liczba.toFloat();
    in>>liczba;
    color[1]=liczba.toFloat();
    in>>liczba;
    color[2]=liczba.toFloat();
    in>>liczba;
    color[3]=liczba.toFloat();
    oblicz_pos();
}

Pocisk::~Pocisk(){}
void Pocisk::rysuj()
{
    glDisable(GL_TEXTURE_2D);
    glColor4fv(color);
    glBegin(GL_POLYGON);
    glVertex2fv(pos1);
    glVertex2fv(pos2);
    glVertex2fv(pos3);
    glVertex2fv(pos4);
    glEnd();
    glEnable(GL_TEXTURE_2D);
}
void Pocisk::oblicz_pos()
{
    pos1[0]=pos[0]-0.5*r;
    pos2[0]=pos[0]+0.5*r;
    pos3[0]=pos[0]+0.5*r;
    pos4[0]=pos[0]-0.5*r;
    pos1[1]=pos[1]+r;
    pos2[1]=pos[1]+r;
    pos3[1]=pos[1]-r;
    pos4[1]=pos[1]-r;
}
Wrog1::Wrog1():Obiekt()
{
    for(int i=0;i<2;i++)
    {
        pos4[i]=0;
        pos5[i]=0;
        pos6[i]=0;
    }
    load_tekstura("t1.png");
}

Wrog1::Wrog1(QString dane)
{
    setDane(dane);
    load_tekstura("t1.png");
}

Wrog1::~Wrog1(){}
void Wrog1::rysuj()
{
    glBindTexture(GL_TEXTURE_2D,tekstura);
    glColor4f(1,1,1,1);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.5,1);
    glVertex2fv(pos3);
    glTexCoord2f(0,0);
    glVertex2fv(pos1);
    glTexCoord2f(0.25,0.25);
    glVertex2fv(pos4);
    glTexCoord2f(0.5,0.15);
    glVertex2fv(pos5);
    glTexCoord2f(0.75,0.25);
    glVertex2fv(pos6);
    glTexCoord2f(1,0);
    glVertex2fv(pos2);
    glEnd();
}
void Wrog1::oblicz_pos()
{
    pos1[0]=pos[0]-r;
    pos2[0]=pos[0]+r;
    pos3[0]=pos[0];
    pos1[1]=pos[1]+r;
    pos2[1]=pos[1]+r;
    pos3[1]=pos[1]-r;

    pos4[0]=pos[0]-0.5*r;
    pos4[1]=pos[1]+0.5*r;
    pos5[0]=pos[0];
    pos5[1]=pos[1]+0.7*r;
    pos6[0]=pos[0]+0.5*r;
    pos6[1]=pos[1]+0.5*r;

}
Wrog2::Wrog2():Obiekt()
{
    for(int i=0;i<2;i++)
    {
        pos4[i]=0;
        pos5[i]=0;
        pos6[i]=0;
        pos7[i]=0;
        pos8[i]=0;
    }
    load_tekstura("t2.png");
}
Wrog2::Wrog2(QString dane)
{
    load_tekstura("t2.png");
    setDane(dane);
}
Wrog2::~Wrog2(){}
void Wrog2::rysuj()
{
    glBindTexture(GL_TEXTURE_2D,tekstura);
    glColor4f(1,1,1,1);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.5,1);
    glVertex2fv(pos3);
    glTexCoord2f(0,0);
    glVertex2fv(pos1);
    glTexCoord2f(0.35,0.3);
    glVertex2fv(pos4);
    glTexCoord2f(0.32,0.1);
    glVertex2fv(pos5);
    glTexCoord2f(0.5,0.25);
    glVertex2fv(pos6);
    glTexCoord2f(0.68,0.1);
    glVertex2fv(pos7);
    glTexCoord2f(0.65,0.3);
    glVertex2fv(pos8);
    glTexCoord2f(1,0);
    glVertex2fv(pos2);
    glEnd();
}
void Wrog2::oblicz_pos()
{
    pos1[0]=pos[0]-r;
    pos2[0]=pos[0]+r;
    pos3[0]=pos[0];
    pos1[1]=pos[1]+r;
    pos2[1]=pos[1]+r;
    pos3[1]=pos[1]-r;

    pos4[0]=pos[0]-0.3*r;
    pos4[1]=pos[1]+0.4*r;
    pos5[0]=pos[0]-0.35*r;
    pos5[1]=pos[1]+0.8*r;
    pos6[0]=pos[0];
    pos6[1]=pos[1]+0.5*r;
    pos7[0]=pos[0]+0.35*r;
    pos7[1]=pos[1]+0.8*r;
    pos8[0]=pos[0]+0.3*r;
    pos8[1]=pos[1]+0.4*r;
}

Wrog3::Wrog3():Obiekt()
{
    for(int i=0;i<2;i++)
    {
        pos4[i]=0;
        pos5[i]=0;
        pos6[i]=0;
        pos7[i]=0;
        pos8[i]=0;
        pos9[i]=0;
        pos10[i]=0;
        pos11[i]=0;
    }
    load_tekstura("t3.png");
}
Wrog3::Wrog3(QString dane)
{
    load_tekstura("t3.png");
    setDane(dane);
}
Wrog3::~Wrog3(){}
void Wrog3::rysuj()
{
    glBindTexture(GL_TEXTURE_2D,tekstura);
    glColor4f(1,1,1,1);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.5,1);
    glVertex2fv(pos3);
    glTexCoord2f(0,0);
    glVertex2fv(pos1);
    glTexCoord2f(0.3,0.35);
    glVertex2fv(pos4);
    glTexCoord2f(0.25,0.08);
    glVertex2fv(pos5);
    glTexCoord2f(0.45,0.25);
    glVertex2fv(pos6);
    glTexCoord2f(0.42,0.15);
    glVertex2fv(pos7);
    glTexCoord2f(0.58,0.15);
    glVertex2fv(pos8);
    glTexCoord2f(0.55,0.25);
    glVertex2fv(pos9);
    glTexCoord2f(0.75,0.08);
    glVertex2fv(pos10);
    glTexCoord2f(0.7,0.35);
    glVertex2fv(pos11);
    glTexCoord2f(1,0);
    glVertex2fv(pos2);
    glEnd();
}
void Wrog3::oblicz_pos()
{
    pos1[0]=pos[0]-r;
    pos2[0]=pos[0]+r;
    pos3[0]=pos[0];
    pos1[1]=pos[1]+r;
    pos2[1]=pos[1]+r;
    pos3[1]=pos[1]-r;

    pos4[0]=pos[0]-0.4*r;
    pos4[1]=pos[1]+0.3*r;
    pos5[0]=pos[0]-0.5*r;
    pos5[1]=pos[1]+0.85*r;
    pos6[0]=pos[0]-0.1*r;
    pos6[1]=pos[1]+0.5*r;
    pos7[0]=pos[0]-0.15*r;
    pos7[1]=pos[1]+0.7*r;
    pos8[0]=pos[0]+0.15*r;
    pos8[1]=pos[1]+0.7*r;
    pos9[0]=pos[0]+0.1*r;
    pos9[1]=pos[1]+0.5*r;
    pos10[0]=pos[0]+0.5*r;
    pos10[1]=pos[1]+0.85*r;
    pos11[0]=pos[0]+0.4*r;
    pos11[1]=pos[1]+0.3*r;
}

Gracz::Gracz():Obiekt()
{
    hp=0;
    load_tekstura("t4.png");
    last_r=0;
}
void Gracz::setGracz(QString dane)
{
    QTextStream in(&dane);
    QString liczba;
    in>>liczba;
    r=liczba.toFloat();
    in>>liczba;
    pos[0]=liczba.toFloat();
    in>>liczba;
    pos[1]=liczba.toFloat();
    in>>liczba;
    hp=liczba.toInt();
    oblicz_pos();
    if(last_r!=r)
    {
        if(r==0.036f)
            load_tekstura("t5.png");
        if(r==0.043f)
            load_tekstura("t6.png");
    }
    last_r=r;
}
Gracz::~Gracz(){}
void Gracz::rysuj()
{
    glBindTexture(GL_TEXTURE_2D,tekstura);
    glColor4f(1,1,1,1);
    glBegin(GL_POLYGON);
    glTexCoord2f(0,0);
    glVertex2fv(pos1);
    glTexCoord2f(1,0);
    glVertex2fv(pos2);
    glTexCoord2f(0.5,1);
    glVertex2fv(pos3);
    glEnd();
    rysuj_hp();
}
void Gracz::rysuj_hp()
{
    glDisable(GL_TEXTURE_2D);
    glColor4f(0,1,0,1);
    glBegin(GL_POLYGON);
    glVertex2f(pos[0]-r,pos[1]-1.2*r);
    glVertex2f(pos[0]-r+2*r*hp*0.0005,pos[1]-1.2*r);
    glVertex2f(pos[0]-r+2*r*hp*0.0005,pos[1]-1.4*r);
    glVertex2f(pos[0]-r,pos[1]-1.4*r);
    glEnd();

    glColor4f(1,0,0,1);
    glBegin(GL_POLYGON);
    glVertex2f(pos[0]-r,pos[1]-1.2*r);
    glVertex2f(pos[0]+r,pos[1]-1.2*r);
    glVertex2f(pos[0]+r,pos[1]-1.4*r);
    glVertex2f(pos[0]-r,pos[1]-1.4*r);
    glEnd();
    glEnable(GL_TEXTURE_2D);
}

void Gracz::oblicz_pos()
{
    pos1[0]=pos[0]-r;
    pos2[0]=pos[0]+r;
    pos3[0]=pos[0];
    pos1[1]=pos[1]-r;
    pos2[1]=pos[1]-r;
    pos3[1]=pos[1]+r;
}
