#ifndef OBIEKT_H
#define OBIEKT_H

#include <QString>
#include <QTextStream>
#include <QGLWidget>

class Obiekt
{
protected:
    float pos[2];
    float pos1[2];
    float pos2[2];
    float pos3[2];
    float r;
    GLuint tekstura;
    QImage plik;
    int w,h;
public:
    Obiekt();
    virtual ~Obiekt();
    virtual void rysuj()=0;
    virtual void oblicz_pos()=0;
    void load_tekstura(QString adres);
    void setDane(QString dane);
};
class Tlo: public Obiekt
{
protected:
    int typ;
    int last_typ;
public:
    Tlo();
    ~Tlo();
    virtual void rysuj();
    virtual void oblicz_pos();
    void setTlo(int typ_);
};

class Pocisk: public Obiekt
{
protected:
    float pos4[2];
    float color[4];
public:
    Pocisk();
    Pocisk(QString dane);
    void setPocisk(QString dane);
    ~Pocisk();
    virtual void rysuj();
    virtual void oblicz_pos();

};

class Wrog1: public Obiekt
{
protected:
    float pos4[2];
    float pos5[2];
    float pos6[2];
public:
    Wrog1();
    Wrog1(QString dane);
    ~Wrog1();
    virtual void rysuj();
    virtual void oblicz_pos();
};

class Wrog2: public Obiekt
{
protected:
    float pos4[2];
    float pos5[2];
    float pos6[2];
    float pos7[2];
    float pos8[2];
public:
    Wrog2();
    Wrog2(QString dane);
    ~Wrog2();
    virtual void rysuj();
    virtual void oblicz_pos();
};

class Wrog3: public Obiekt
{
protected:
    float pos4[2];
    float pos5[2];
    float pos6[2];
    float pos7[2];
    float pos8[2];
    float pos9[2];
    float pos10[2];
    float pos11[2];
public:
    Wrog3();
    Wrog3(QString dane);
    ~Wrog3();
    virtual void rysuj();
    virtual void oblicz_pos();
};

class Gracz: public Obiekt
{
protected:
    int hp;
    float last_r;
public:
    Gracz();
    void setGracz(QString dane);
    ~Gracz();
    virtual void rysuj();
    void rysuj_hp();
    virtual void oblicz_pos();
};

#endif // OBIEKT_H
