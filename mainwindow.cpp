#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    socket=new QTcpSocket(this);
    connect(socket,SIGNAL(readyRead()),this,SLOT(dataReceived()));

    w=false;
    s=false;
    d=false;
    a=false;
    l=false;
}
MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::dataReceived()
{
    klawisze();
    QTextStream in_serwer(socket);
    QString data_serwer;
    data_serwer=in_serwer.readAll();
    QTextStream in(&data_serwer);
    QString data;
    int n_obiektow[4];
    int n=0;
    data=in.readLine();
    ui->glWidget->setTlo(data);
    if(data.toInt()!=4)
    {
    data=in.readLine();
    while(data[0]!='.')
    {
        ui->glWidget->dodajPocisk(data,n);
        n++;
        data=in.readLine();
    }
    n_obiektow[0]=n;
    n=0;
    data=in.readLine();
    while(data[0]!='.')
    {
        ui->glWidget->dodajWrog1(data,n);
        n++;
        data=in.readLine();
    }
    n_obiektow[1]=n;
    n=0;
    data=in.readLine();
    while(data[0]!='.')
    {
        ui->glWidget->dodajWrog2(data,n);
        n++;
        data=in.readLine();
    }
    n_obiektow[2]=n;
    n=0;
    data=in.readLine();
    while(data[0]!='.')
    {
        ui->glWidget->dodajWrog3(data,n);
        n++;
        data=in.readLine();
    }
    n_obiektow[3]=n;
    ui->glWidget->usun(n_obiektow);
    data=in.readLine();
    ui->glWidget->dodajGracz1(data);
    data=in.readLine();
    ui->glWidget->dodajGracz2(data);
    data=in.readLine();
    ui->lcdNumber->display(data);
    }
    ui->glWidget->updateGL();
}

void MainWindow::keyPressEvent(QKeyEvent *k)
{
    QString keys=k->text();
    for(int i=0;i<keys.size();i++)
    {
        switch(keys[i].unicode())
        {
        case 'w':
        {
            w=true;
            break;
        }
        case 's':
        {
            s=true;
            break;
        }
        case 'd':
        {
            d=true;
            break;
        }
        case 'a':
        {
            a=true;
            break;
        }
        case 'l':
        {
            l=true;
            break;
        }
        }
    }
}
void MainWindow::keyReleaseEvent(QKeyEvent *k)
{
    QString keys=k->text();
    for(int i=0;i<keys.size();i++)
    {
        switch(keys[i].unicode())
        {
        case 'w':
        {
            w=false;
            break;
        }
        case 's':
        {
            s=false;
            break;
        }
        case 'd':
        {
            d=false;
            break;
        }
        case 'a':
        {
            a=false;
            break;
        }
        case 'l':
        {
            l=false;
            break;
        }
        }
    }
}
void MainWindow::on_pushButton_clicked()
{
    socket->abort();
    socket->connectToHost(ui->adres->text(),ui->port->text().toInt());
}
void MainWindow::klawisze()
{
    QTextStream out(socket);
    if(w)
    out<<"1";
    if(s)
    out<<"2";
    if(d)
    out<<"3";
    if(a)
    out<<"4";
    if(l)
    out<<"5";
}
